---SUMMARY---

This module simply send an email from the website.

For a full description visit project page:
https://www.drupal.org/project/support_mail

Bug reports, feature suggestions and latest developments:
http://drupal.org/project/issues/support_mail

---INTRODUCTION---


None.


---REQUIREMENTS---


*None. (Other than a clean Drupal installation)


---INSTALLATION---

Install as usual. Place the entirety of this directory in the /modules 
folder of your Drupal installation. 

---CONFIGURATION---

None.


---CONTACT---

Current Maintainers:
*Balogh Zoltán (zlyware) - https://www.drupal.org/u/u/zoltán-balogh
